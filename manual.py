#!/usr/bin/env python3

import time
from pygame.locals import (K_RIGHT, K_LEFT, K_UP, K_DOWN,
                           K_q, KEYDOWN, QUIT)

from autobotmaze import Runner

def callback(position):
    # sleep for a millisecond, just to avoid flogging the cpu
    position.pygame.event.clear()
    while True:
        event = position.pygame.event.wait()
        if event.type == QUIT:
            position.pygame.quit()
            sys.exit()
        elif event.type == KEYDOWN:
            if event.key == K_RIGHT:
                return 'E'
            elif event.key == K_LEFT:
                return 'W'
            elif event.key == K_UP:
                return 'N'
            elif event.key == K_DOWN:
                return 'S'
            elif event.key == K_q:
                return 'Q'

if __name__ == '__main__':
    app = Runner(callback)
    app.run()
