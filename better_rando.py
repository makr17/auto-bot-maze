#!/usr/bin/env python3

from random import randint
import time
from autobotmaze import Runner

last_move = None
inverses = {
    'N': 'S',
    'S': 'N',
    'E': 'W',
    'W': 'E'
    }

def callback(position):
    global last_move

    # sleep for a bit, so we can follow along
    time.sleep(0.2)
    moves = []
    for move in ['N', 'S', 'E', 'W']:
        # short-circuit if we're next to the exit
        if position.is_exit(move):
            # yay!
            return move
        # check to see if this move would negate the previous
        if last_move and move == inverses[last_move]:
            # skip
            continue
        # check to make sure that the move is valid (no wall)
        if position.valid(move):
            # move is valid, add it to the list of possibilities
            moves.append(move)

    # only available move is to turn around
    if len(moves) == 0:
        moves.append(inverses[last_move])

    # randomly pick one of the valid moves we selected above
    move = moves[randint(0, len(moves)-1)]
    # and populate last_move for next time
    last_move = move
    return move


if __name__ == '__main__':
    app = Runner(callback)
    app.run()
