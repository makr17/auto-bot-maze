# Auto Bot Maze

Autonomous Robot Maze

Teaching platform to introduce students to programming in python.  Fill out callback stub to navigate a virtual robot through a randomly-generated maze.  Originally created for [HopeIT](https://hopeit.net), hopefully others will find it useful as well.

The provided callback is called at each step of the game.

The interface presented to the callback is fairly simple.
* it receives a `Position` object
  * has attributes `x` and `y` for the current position
  * also composes the pygame instance as `pygame` in case you
    need/want pygame resources (examples/manual.py makes use of this
    for keyboard input)
  * exposes a `valid()` method
    * `valid('N')` will return `True` if North is a valid move from
      the current position
    * likewise for 'S', 'E', and 'W'
  * and an `is_exit()` method
    * `is_exit('N')` returns `True` if the exit is directly North of
      the current position.
    * likewise for 'S', 'E', and 'W'
* the callback is expected to return a direction to move
  * one of 'N', 'S', 'E' or 'W'
  * returning 'Q' causes the program to exit
* when the bot moves to the exit, the program exits


`examples/` contains this working example
* `manual.py` accepts keyboard input to move the bot.  Good for early
  exploration and discovery.  Try with the `--hide` flag to increase
  the difficulty by hiding the exit until the bot is adjacent to it.
* `rando.py` picks a random direction at every turn, often a
  direction that is not a valid move.  Not very effective, but
  amusing, and just about the simplest example (short of "always move
  North")
* `better_rando.py` first determines valid moves, then makes a random
  pick from that set.  Avoids backtracking unless there is no other
  valid move to make.  More effective than you might think, makes a
  good starting point to experiment with other algorithms for solving
  the maze.


Command line flags
* `--height` sets the height of the maze, defaults to 21
* `--width` sets the width, defaluts to 35
* `--hide` hides the exit until the bot is adjacent to it
* `--seed` provides a random seed, useful for retrying a maze after
  tweaking your algorithm.
* `--complexity` and `--density` tweak the internals of the maze generator.

Highlights/TODO:

- [x] build maze from random seed, pick exit and starting point based on that seed
- [x] output seed, and accept it as input
  - this way a student can retry an exact maze they failed before after tweaking bot code
- [x] variable scale on the generated mazes
  - so that we can generate small/easy mazes as well as large/complex
- [ ] maybe: flag to generate maze not solvable by right hand rule
  - this might be harder given the random generation
  - isn't just the maze structure, involves a cycle and starting placement
- [x] structured as a library that can be invoked with code built to its interface
- [x] I think the exit shouldn’t need to be on the edge, so pick an x,y (that isn’t blocked) and make that an exit
- [x] same with starting point, pick an x,y.
  - [ ] thought:  make the starting point and the exit be in separate quadrants of the maze
- [x] runner class
  - [x] composes the Maze object
  - [x] and validates moves from the callback it is given
  - [x] student can’t get to the maze object directly to ask it for the exit coordinates (or, if they can, maybe they’re in the wrong class)
  - [x] need to come up with a first stab on the interface that the callback receives…
    - [x] probably a Location object
        - [x] exposes x, y, and walls to n, s, e, w.  (any other data)
- [x] display using pygame
  - [x] SVG for graphics, and auto-scale for the screen dimensions
  - [x] multiple robot sprites?
    - [ ] add to Runner an option to specify which bot to use
    - [x] randomly pick a bot at runtime unless specified
- [x] sizing beyond explicit x,y.  something like t-shirt sizes, S, M, L, XL...

Dependencies:

* numpy
* pygame

If you are using MacOS Mojave, binary builds of pygame may not work for you
(blank output).  If this happens to you, see [This answer on StackOverflow](https://stackoverflow.com/a/59060598)
for necessary dependencies and instructions on building from source.

Credits:

Most SVG assets courtesy of [svgrepo.com](svgrepo.com)

* [Brick Wall SVG](https://www.svgrepo.com/svg/67462/brick-wall)
* [Door SVG](https://www.svgrepo.com/svg/51980/door)
* Guard SVGs
  * [01](https://www.svgrepo.com/svg/81654/robot)
  * [01](https://www.svgrepo.com/svg/106387/robot)
* Robot SVGs
  * [01](https://www.svgrepo.com/svg/62629/robot)
  * [02](https://www.svgrepo.com/svg/11338/robot)
  * [03](https://www.svgrepo.com/svg/62629/robot)
  * [04](https://www.svgrepo.com/svg/30596/robot)
  * [05](https://www.svgrepo.com/svg/162226/robot)
  * [06](https://www.svgrepo.com/svg/230299/robot)
  * [07](https://www.svgrepo.com/svg/296552/robot)

black-square.svg (as simple as it is) is our own work.

All SVG assets are licensed [Creative Commons BY 4.0](https://creativecommons.org/licenses/by/4.0/)
