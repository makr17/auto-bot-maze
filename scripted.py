#!/usr/bin/env python3

import time
from autobotmaze import Runner

moves = []
def callback(position):
    time.sleep(2.0)
    global moves
    move = moves.pop(0)
    return move

if __name__ == '__main__':
    app = Runner(callback, 1)
    app.run()
