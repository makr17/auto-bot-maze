#!/usr/bin/env python3

import time
from autobotmaze import Runner

hand = {
    'N': ['E', 'N', 'W', 'S'],
    'S': ['W', 'S', 'E', 'N'],
    'E': ['S', 'E', 'N', 'W'],
    'W': ['N', 'W', 'S', 'E']
}

last_move = 'N'
been_there = {}

# track moves and visit times
# to drive logic if we have no unvisited move candidates
class Visit(object):
    def __init__(self, time, move):
        self.time = time
        self.move = move
    # needed for sorting
    def __lt__(self, other):
        return self.time < other.time

def callback(position):
    global last_move
    global been_there
    # sleep for a bit, so we can follow along
    time.sleep(0.02)
    # record current position and time for tracking
    current = position.current()
    been_there[current] = time.time()
    # move list for right-hand-rule given current direction
    moves = hand[last_move]
    # look for the exit first
    for move in moves:
        if position.is_exit(move):
            last_move = move
            return move
    # then employ the right-hand-rule
    # encoded in the list of moves from hand dictionary
    # skip moves we've already visited
    for move in moves:
        if position.valid(move) and not position.point(move) in been_there:
            last_move = move
            return move
    # if we get this far then there are no unvisited candidates
    # so take a look at the valid move options
    # and store them with visit time from been_there
    valid = []
    for move in moves:
        if position.valid(move):
            # time first in the tuple, so following sort will be time-first
            valid.append(Visit(been_there[position.point(move)], move))
    # sort valid moves by the time last visited
    # oldest visit first
    valid.sort()
    # and take the first (oldest) option
    last_move = valid[0].move
    return last_move


if __name__ == '__main__':
    app = Runner(callback)
    app.run()
