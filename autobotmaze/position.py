class Position:
    x = None
    y = None

    _maze = None
    pygame = None

    def __init__(self, maze, x, y):
        self.x = x
        self.y = y
        self._maze = maze

    def current(self):
        return (self.y, self.x)

    def point(self, direction):
        cur = self.current()
        offset = self._maze.direction_offsets[direction]
        move = tuple(map(sum, zip(cur, offset)))
        return move

    def valid(self, direction):
        (y,x) = self.point(direction)
        if self._maze.maze[y, x] == 1:
            return False
        return True

    def blocked(self, direction):
        return not self.valid(direction)

    def is_exit(self, direction):
        (y,x) = self.point(direction)
        if y == self._maze.exit_y and x == self._maze.exit_x:
            return True
        return False

    def move(self, direction):
        (y,x) = self.point(direction)
        self.x = x
        self.y = y
