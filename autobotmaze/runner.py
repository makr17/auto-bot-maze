import argparse
import io
from numpy.random import randint
import os
os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"
from os import listdir
from os.path import isfile, join
from pathlib import Path
from pygame.locals import *  # noqa
import pygame
import re
import time

from .maze import Maze
from .position import Position
from .guard import Guard

class Runner:
    pygame = None
    _maze = None
    position = None

    callback = None

    _scale = 32

    windowWidth = None
    windowHeight = None
    surfaces = {
        'display': None,
        'robot': None,
        'block': None,
        'door':  None,
        'guard': None
    }
        
    _guards = []
    
    fill = (240, 240, 240)

    sizes = {
        'S': (15, 11),
        'M': (31, 21),
        'L': (55, 31),
        'XL':(93, 53)
    }

    # hash from (y,x) positions to number of times visited
    visited = {}

    _type_stripper = re.compile("\.[^\.]+$")
    
    def __init__(self, callback, seed=None):
        self._args = self.parse_args()
        if seed is not None:
            self._args["seed"] = seed
        self._running = True
        pygame.init()
        self.pygame = pygame
        self.callback = callback

        # setup the maze
        # only send Maze args that it cares about
        maze_args = ['width', 'height', 'complexity', 'density', 'seed'];
        margs = {k:self._args[k] for k in maze_args}
        self._maze = Maze(**margs)

        self._pick_image_format()
        print(f"image format={self.image_asset_format}")
        # setup visuals
        self.load_visual_assets(self._maze)
        
        # pick a starting point
        self.position = self.pick_start_position(self._maze)
        # position guards
        for i in range(0, self._args['guards']):
            self._guards.append(Guard(self.pick_start_position(self._maze)))
        
        self._running = True
        print('random seed =', self._maze.seed)

    def _pick_image_format(self):
        # assets are either in current directory or one level up
        if os.path.isdir("assets"):
            self.assetdir = "assets"
        elif os.path.isdir("../assets"):
            self.assetdir = "../assets"
        print(f"assetdir: {self.assetdir}")
        # try to load in order of preference
        # for some reason svg doesn't work with windows pygame wheel
        for format in ['svg', 'png', 'tif', 'bmp']:
            try:
                self.load_image(f"{self.assetdir}/brick", fit_to=None, format=format)
            except:
                print(f"failed to load {format}")
            else:
                self.image_asset_format = format
                break

    def load_visual_assets(self, maze):
        # make the window as big as possible
        info = pygame.display.Info()
        self._scale = int(sorted([(info.current_h-40)/maze.height, (info.current_w-40)/maze.width])[0])
        self.windowWidth = self._maze.width * self._scale
        self.windowHeight = self._maze.height * self._scale
        self.surfaces['display'] = pygame.display.set_mode(
            (self.windowWidth, self.windowHeight),
            pygame.HWSURFACE|pygame.DOUBLEBUF, 32)
        pygame.display.set_caption('Auto Robot Maze')

        # load and scale the visual assets
        fit = (self._scale, self._scale)
        self.surfaces['robot'] = self.load_image(self.pick_from_many('robot'), fit_to=fit)
        self.surfaces['block'] = self.load_image(f"{self.assetdir}/{self._args['wall']}", fit_to=fit)
        self.surfaces['door']  = self.load_image(f"{self.assetdir}/door", fit_to=fit)
        self.surfaces['guard'] = self.load_image(self.pick_from_many('guard'), fit_to=fit)

    def pick_from_many(self, type):
        path = join(self.assetdir, type)
        files = [join(path, f) for f in listdir(path) if isfile(join(path, f))]
        file = files[randint(0, len(files))]
        file = re.sub(self._type_stripper, "", file)
        return file                         
        
    def pick_start_position(self, maze):
        x = randint(1, maze.width - 1)
        y = randint(1, maze.height - 1)
        while self._maze.maze[y, x]:
            # shift randomly left or right (or maybe stay put)
            x = x + randint(-1, 1)
            # make sure we didn't run off the edge
            if x < 1:
                x = 1
            if x >= self._maze.width:
                x = self._maze.width - 1
            # shift up or down (or maybe stay put)
            y = y + randint(-1, 1)
            # make sure we didn't run off the edge
            if y < 1:
                y = 1
            if y >= self._maze.height:
                y = self._maze.height - 1
        position = Position(self._maze, x, y)
        position.pygame = pygame
        return position
        
    def parse_args(self):
        parser = argparse.ArgumentParser(description='generate a random maze')
        parser.add_argument('--seed', metavar='s', dest='seed', type=int,
                            help='integer seed for maze generation')
        parser.add_argument('--size', dest='size', type=str,
                            help='maze size [S, M, L, XL]')
        parser.add_argument('--wall', dest='wall', type=str,
                            help='which svg icon to use for wall segments',
                            default='wall')
        parser.add_argument('--width', metavar='w', dest='width', type=int,
                            default=15, help='maze width')
        parser.add_argument('--height', metavar='y', dest='height', type=int,
                            default=11, help='maze height')
        parser.add_argument('--complexity', metavar='c', dest='complexity', type=float,
                            default=0.75, help='maze complexity')
        parser.add_argument('--density', metavar='d', dest='density', type=float,
                            default=0.75, help='density for the maze')
        parser.add_argument(
            '--visits',
            dest='visits',
            help='track and show visited cells',
            default=False,
            action='store_true'
        )
        parser.add_argument(
            '--hide',
            dest='hide',
            help='hide exit until next to it',
            default=False,
            action='store_true'
        )
        parser.add_argument('--guards', metavar='g', dest='guards', type=int,
                            default=0, help='how many guards in the maze')
        args = parser.parse_args()
        args_dict = vars(args)
        # override w/h if t-shirt size is specified
        if 'size' in args_dict:
            if args_dict['size'] in self.sizes:
                (w,h) = self.sizes[args_dict['size']]
                args_dict['width'] = w
                args_dict['height'] = h
        return args_dict


    def cleanup(self):
        pygame.quit()

    def run(self):
        while(self._running):
            self.move_guards()
            self.render()
            pygame.event.pump()
            # track number of visits to this position
            point = self.position.current()
            if point not in self.visited:
                self.visited[point] = 1
            else:
                self.visited[point] += 1
            m = self.callback(self.position)
            self.move(self.position, m)

        self.cleanup()

    def move_guards(self):
        for guard in self._guards:
            guard.move()
            
    def render(self):
        self.surfaces['display'].fill(self.fill)
        self.surfaces['display'].blit(self.surfaces['robot'],
                                (self.position.x * self._scale,
                                 self.position.y * self._scale))
        for x in range(0, self._maze.width):
            for y in range(0, self._maze.height):
                if self._maze.maze[y, x]:
                    self.surfaces['display'].blit(self.surfaces['block'],
                                            (x * self._scale, y * self._scale))
                elif self._args['visits'] and (y,x) in self.visited:
                    pygame.draw.rect(
                        self.surfaces['display'],
                        [0, 255, 50],
                        pygame.Rect(
                            x * self._scale, y * self._scale,
                            self._scale, self._scale
                        ),
                        self.visited[(y,x)]
                    )
        # if enabled, hide the exit if we're not next to it
        if not self._args['hide'] or (abs(self.position.x - self._maze.exit_x) <= 1 and abs(self.position.y - self._maze.exit_y) <= 1):
            self.surfaces['display'].blit(self.surfaces['door'],
                                    (self._maze.exit_x * self._scale,
                                    self._maze.exit_y * self._scale))
        for guard in self._guards:
            self.surfaces['display'].blit(self.surfaces['guard'],
                                (guard.position.x * self._scale,
                                 guard.position.y * self._scale))
        pygame.display.flip()

    def flash_red(self):
        self.surfaces['display'].fill((255, 0, 0))
        pygame.display.flip()
        time.sleep(0.05)
        self.render()

    # TODO: maybe this is a better fit under Position?
    def move(self, pos, move):
        # stop if move is 'Q'
        # TODO: maybe if we attempt any move but [N, S, E, W]?
        if move == 'Q':
            self._running = False
        # stop if we reach the exit
        elif pos.is_exit(move):
            self._running = False
        # if the move is valid, make it happen
        elif pos.valid(move):
            pos.move(move)
        else:
            self.flash_red()

    def load_svg(self, path, fit_to=None):
        svg = Path(path).read_text()
        surface = pygame.transform.scale(
            pygame.image.load(
                io.BytesIO(
                    svg.encode()
                )
            ),
            fit_to
        )
        return surface

    def load_svg_asset(self, path):
        svg = Path(f"{path}.svg").read_text()
        return io.BytesIO(svg.encode())

    def load_image(self, path, fit_to=None, format=None):
        if format == None:
            format = self.image_asset_format
        # svg needs a bit of special handling
        if format == 'svg':
            img = self.load_svg_asset(path)
        # other image formats should work just fine with a path
        else:
            img = f"{path}.{format}"
        surface = pygame.image.load(img)
        if fit_to != None:
            return pygame.transform.scale(surface, fit_to)
        return surface
