# Welcome

# How to Contribute

## Building locally

We recommend initialing a virtual environment to install packaged
locally and then installing an editable copy of this package

  ```
  virtualenv -p `which python3` env
  . env/bin/activate
  pip3 install -e .
  ```

if you are using a mac and homebrew, the following should work

  ```
  brew install pyenv-virtualenv
  eval "$(pyenv virtualenv-init -)"
  cd auto-bot-maze
  pip3 install -e .
  ```
